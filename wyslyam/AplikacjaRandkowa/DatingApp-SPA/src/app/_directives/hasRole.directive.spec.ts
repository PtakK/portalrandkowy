/* tslint:disable:no-unused-variable */

import { TestBed, async } from '@angular/core/testing';
import { HasRoleDirective } from './hasRole.directive';
import { ViewContainerRef } from '@angular/core';

describe('Directive: HasRole', () => {
  it('should create an instance', () => {
    const directive = new HasRoleDirective(null,null,null);
    expect(directive).toBeTruthy();
  });
});
